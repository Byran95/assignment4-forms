/*global require, exports, console, __filename, sort */
/**
 * Created by theotheu on 24-12-13.
 */

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Group = mongoose.model('Group'),
    passwordHash = require('password-hash'); // Add this line to file /server/users.js


// CREATE
// save @ http://mongoosejs.com/docs/api.html#model_Model-save
exports.create = function (req, res) {
    "use strict";

    // Encrypt password

    req.body.password = passwordHash.generate(req.body.password || "topSecret!");

    var doc = new User(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);

    });

};

// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    "use strict";

    var conditions, fields, sort;

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    User
        .find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

exports.detail = function (req, res) {
    "use strict";

    var conditions, retDoc, i, groupDoc;

    conditions = req.params._id;

    User
        .findById(conditions, {}, {'createdAt': -1})
        .populate("Group")// <------------------ populating sub documents, only for demo, not needed here
        .exec(function (err, doc) {

            var groups = [];
            // Create a array, so that we can compare all groups with existing groups
            if (!doc) {
                doc = {};
                doc.gender = "male";
                doc.picture = "user.png";
            } else if (doc && doc.groups) {
                for (i = 0; i < doc.groups.length; i++) {
                    groups.push(doc.groups[i]._id);
                }
            }

            // Find groups for user
            Group
                .find({}, {}, function (err, groupsDoc) {
                    retDoc = [];
                    for (i = 0; i < groupsDoc.length; i++) {
                        groupDoc = {
                            _id: groupsDoc[i]._id,
                            name: groupsDoc[i].name,
                            isMember: false
                        };

                        if (groups.indexOf(groupsDoc[i]._id) >= 0) {
                            groupDoc.isMember = true;
                        }
                        retDoc.push(groupDoc);
                    }

                    var retObj = {
                        meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                        doc: doc,
                        groups: retDoc,
                        err: err
                    };
                    return res.send(retObj);
                });
        });
};

// Nested callback for $addToSet
function updateGroupsWithUser(err, req, res, groups, doc) {
    "use strict";

    var group, retObj;

    if (groups && groups.length > 0) {
        // Get first element from groups array.
        group = groups.pop();

        // Check if group has to be added or excluded from groups based on attribute "isMember"
        if (group.isMember) {
            // Add to set
            User
                .update({_id: doc._id}, {$addToSet: {"groups": group}}, function (res1) {
                    updateGroupsWithUser(err, req, res, groups, doc);
                });
        } else {
            // Remove from set
            User
                .update({_id: doc._id}, {$pull: {"groups": group}}, function (res1) {
                    updateGroupsWithUser(err, req, res, groups, doc);
                });
        }
    } else {
        // Return if no array or empty array (consider alternative $pullAll)
        retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }
}


// UPDATE
// findOneAndUpdate @ http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
exports.update = function (req, res) {
    "use strict";

    var conditions, update, retObj;

    // Password validation. Can only partially be a validation rule from the model because of confirmPassword.
    if (req.body.password && req.body.password !== '' && (req.body.password.length < 8 || req.body.password !== req.body.confirmPassword)) {
        retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: null,
            err: {
                message: "Passwords must be the same and at least 8 characters. Please verify your password."
            }
        };
        return res.send(retObj);
    }

    conditions = req.params._id;
    update = {
        email: req.body.email || '',
        modificationDate: Date.now()
    };


    if (req.body.password && req.body.password !== '') {
        // Make sure that password is hashed.
        update.password = passwordHash.generate(req.body.password || "topSecret!");
    }

    User
        .findByIdAndUpdate(conditions, update, {multi: false}, function (err, doc) {
            updateGroupsWithUser(err, req, res, req.body.groups, doc);
        });
};

// DELETE
// remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
exports.delete = function (req, res) {
    "use strict";
    var conditions, retObj;

    conditions = {_id: req.params._id};

    User.remove(conditions, function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
