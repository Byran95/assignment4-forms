/**
 * Created by theotheu on 09-10-14.
 */
/*jslint node: true */

"use strict";

var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var session = require('express-session');
var cookieParser = require('cookie-parser');
var app = express();                                // Create the Express app

// Load configuration
var env = process.env.NODE_ENV || 'development',
    config = require('./config/config.js')[env];


// Bootstrap db connection
mongoose.connect(config.db);

mongoose.connection.on('error', function (err) {
    "use strict";
    console.error('MongoDB error: %s', err);
});

// Set debugging on/off
if (config.debug) {
    mongoose.set('debug', true);
} else {
    mongoose.set('debug', false);
}

// Bootstrap models. Read all models from the model directory
var models_path = __dirname + '/app/models',
    model_files = fs.readdirSync(models_path);
model_files.forEach(function (file) {
    require(models_path + '/' + file);
});

// App settings
app.set('port', process.env.PORT || config.port);       // Set the port

app.use(cookieParser());                                // Read cookies (needed for auth)
app.use(bodyParser.json());                             // Configure body-parser with JSON input
app.use(bodyParser.urlencoded({extended: true}));       // Notice because option default will flip in next major; http://goo.gl/bXjyyz

// Bootstrap routes
var routes_path = __dirname + '/routes',
    route_files = fs.readdirSync(routes_path);

route_files.forEach(function (file) {
    "use strict";
    require(routes_path + '/' + file)(app);
});

app.all('*', function (req, res) {                      // Catch all for unmatched routes
    res.status(404).send('Not Found');
});

module.exports = app;

